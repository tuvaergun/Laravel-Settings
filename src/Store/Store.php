<?php

/*
 * This file is part of Laravel Settings.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Settings\Store;

use DraperStudio\Settings\Contracts\Store as StoreContract;

abstract class Store implements StoreContract
{
    /**
     * The array of stored values.
     *
     * @var array
     */
    protected $storage = [];

    /**
     * Whether the store has changed since it was last loaded.
     *
     * @var bool
     */
    protected $modified = false;

    /**
     * Whether the settings data are loaded.
     *
     * @var bool
     */
    protected $loaded = false;

    /**
     * Get all of the items from the configuration.
     *
     * @return array
     */
    public function all()
    {
        $this->checkLoaded();

        return $this->storage;
    }

    /**
     * Determine if an item exists in the configuration.
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        $this->checkLoaded();

        return !is_null($this->get($key));
    }

    /**
     * Retrieve an item from the configuration by key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get($key)
    {
        $this->checkLoaded();

        if (array_key_exists($key, $this->storage)) {
            return $this->storage[$key];
        }
    }

    /**
     * Store an item in the configuration for a given number of minutes.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function put($key, $value = null)
    {
        $this->checkLoaded();
        $this->modified = true;

        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->put($k, $v);
            }
        } else {
            $this->storage[$key] = $value;
        }
    }

    /**
     * Remove an item from the configuration.
     *
     * @param string $key
     *
     * @return bool
     */
    public function forget($key)
    {
        $this->modified = true;

        if ($this->has($key)) {
            unset($this->storage[$key]);
        }

        return true;
    }

    /**
     * Remove all items from the configuration.
     */
    public function flush()
    {
        $this->storage = [];
        $this->modified = true;
    }

    /**
     * Save any changes done to the settings data.
     */
    public function save()
    {
        if (!$this->modified) {
            return;
        }

        $this->write($this->storage);
        $this->modified = false;
    }

    /**
     * Check if the settings data has been loaded.
     */
    private function checkLoaded()
    {
        if (!$this->modified) {
            $this->storage = json_decode(json_encode($this->read()), true);
            $this->modified = true;
        }
    }

    /**
     * Read the data from the store.
     *
     * @return array
     */
    abstract protected function read();

    /**
     * Write the data into the store.
     *
     * @param array $storage
     */
    abstract protected function write(array $storage);
}
