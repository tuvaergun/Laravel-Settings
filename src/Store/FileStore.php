<?php

/*
 * This file is part of Laravel Settings.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Settings\Store;

use Illuminate\Filesystem\Filesystem;
use DraperStudio\Cerealizer\Contracts\Serialiser;
use DraperStudio\Cerealizer\Contracts\Unserialiser;

abstract class FileStore extends Store
{
    /**
     * Filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Serialiser instance.
     *
     * @var YamlSerialiser
     */
    private $serialiser;

    /**
     * Unserialiser instance.
     *
     * @var YamlUnserialiser
     */
    private $unserialiser;

    /**
     * Constructor.
     *
     * @param Filesystem   $files
     * @param Serialiser   $serialiser
     * @param Unserialiser $unserialiser
     * @param string       $path
     */
    public function __construct(Filesystem $files, Serialiser $serialiser, Unserialiser $unserialiser, $path)
    {
        $this->files = $files;
        $this->serialiser = $serialiser;
        $this->unserialiser = $unserialiser;
        $this->setPath($path);
    }

    /**
     * Read the data from the configuration file.
     *
     * @return array
     */
    protected function read()
    {
        return $this->unserialiser->unserialise($this->files->get($this->path));
    }

    /**
     * Write the data into the configuration file.
     *
     * @param array $storage
     */
    protected function write(array $data)
    {
        return $this->files->put($this->path, $this->serialiser->serialise($data));
    }

    /**
     * Set the path for the JSON file.
     *
     * @param string $path
     */
    private function setPath($path)
    {
        if (!$this->files->exists($path)) {
            $result = $this->files->put($path, '');

            if ($result === false) {
                throw new NotWriteableException("Could not write to $path.");
            }
        }

        if (!$this->files->isWritable($path)) {
            throw new NotWriteableException("$path is not writable.");
        }

        $this->path = $path;
    }
}
