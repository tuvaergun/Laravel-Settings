<?php

/*
 * This file is part of Laravel Settings.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Settings\Store;

class MemoryStore extends Store
{
    /**
     * Constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Read the data from the configuration file.
     *
     * @return array
     */
    protected function read()
    {
        return $this->data;
    }

    /**
     * Write the data into the configuration file.
     *
     * @param array $storage
     */
    protected function write(array $data)
    {
        //
    }
}
