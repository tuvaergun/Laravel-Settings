<?php

/*
 * This file is part of Laravel Settings.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Settings;

use DraperStudio\Cerealizer\Serialisers\JsonSerialiser;
use DraperStudio\Cerealizer\Serialisers\XmlSerialiser;
use DraperStudio\Cerealizer\Serialisers\YamlInlineSerialiser;
use DraperStudio\Cerealizer\Serialisers\YamlSerialiser;
use DraperStudio\Cerealizer\Unserialisers\JsonUnserialiser;
use DraperStudio\Cerealizer\Unserialisers\XmlUnserialiser;
use DraperStudio\Cerealizer\Unserialisers\YamlUnserialiser;
use DraperStudio\Settings\Store\DatabaseStore;
use DraperStudio\Settings\Store\JsonStore;
use DraperStudio\Settings\Store\MemoryStore;
use DraperStudio\Settings\Store\XmlStore;
use DraperStudio\Settings\Store\YamlInlineStore;
use DraperStudio\Settings\Store\YamlStore;
use Illuminate\Support\Manager;

class SettingsManager extends Manager
{
    /**
     * Create a new Default Driver instance.
     */
    public function getDefaultDriver()
    {
        return $this->getConfig('settings.store');
    }

    /**
     * Create a new JSON Driver instance.
     */
    public function createJsonDriver()
    {
        $path = $this->getConfig('settings.path');

        return new JsonStore(
            $this->app['files'], new JsonSerialiser(), new JsonUnserialiser(), $path
        );
    }

    /**
     * Create a new XML Driver instance.
     */
    public function createXmlDriver()
    {
        $path = $this->getConfig('settings.path');

        return new XmlStore(
            $this->app['files'], new XmlSerialiser(), new XmlUnserialiser(), $path
        );
    }

    /**
     * Create a new YAML Driver instance.
     */
    public function createYamlDriver()
    {
        $path = $this->getConfig('settings.path');

        return new YamlStore(
            $this->app['files'], new YamlSerialiser(), new YamlUnserialiser(), $path
        );
    }

    /**
     * Create a new YAML Inline Driver instance.
     */
    public function createYamlInlineDriver()
    {
        $path = $this->getConfig('settings.path');

        return new YamlInlineStore(
            $this->app['files'], new YamlInlineSerialiser(), new YamlUnserialiser(), $path
        );
    }

    /**
     * Create a new Driver instance.
     */
    public function createDatabaseDriver()
    {
        $connection = $this->app['db']->connection();
        $table = $this->getConfig('settings.table');

        return new DatabaseStore($connection, $table);
    }

    /**
     * Create a new Driver instance.
     */
    public function createMemoryDriver()
    {
        return new MemoryStore();
    }

    /**
     * Read a value from the configuration by its key.
     *
     * @param $key
     */
    protected function getConfig($key)
    {
        return $this->app['config']->get($key);
    }
}
