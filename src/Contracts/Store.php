<?php

/*
 * This file is part of Laravel Settings.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DraperStudio\Settings\Contracts;

interface Store
{
    /**
     * Get all of the items from the configuration.
     *
     * @return array
     */
    public function all();

    /**
     * Determine if an item exists in the configuration.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function has($key);

    /**
     * Retrieve an item from the configuration by key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get($key);

    /**
     * Store an item in the configuration for a given number of minutes.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function put($key, $value);

    /**
     * Remove an item from the configuration.
     *
     * @param string $key
     *
     * @return bool
     */
    public function forget($key);

    /**
     * Remove all items from the configuration.
     */
    public function flush();

    /**
     * Save any changes done to the settings data.
     */
    public function save();
}
