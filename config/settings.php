<?php

/*
 * This file is part of Laravel Settings.
 *
 * (c) DraperStudio <hello@draperstud.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    /*
     * Which type of store to use.
     *
     * Valid options are 'json', 'xml', 'yaml', 'yaml_inline' and 'database'.
     */
    'store' => 'yaml_inline',

    /*
     * If the JSON, XML or YAML store is used, give the full path to the file
     * that the store writes to.
     */
    'path' => storage_path('app/configuration.json'),

    /*
     * If the database store is used, give the name of the database
     * table to use.
     */
    'table' => 'settings',
];
